/* Which prime, below one-million, can be written as the sum of the most
 * consecutive primes? */

#include <stdio.h>
#include <stdlib.h>

int findPrime(int* primes,
		const int* primelist,
		const int maxprime,
		const int start,
		int* prime);
int* buildPrimes(const int maxprime);
void buildPrimeList(int* primes, int* primelist, int primecount);

int main()
{
	int maxprime = 0;
	int count = 0;
	const int max = 1e6;
	const int primecount = 1000;
	int* primelist = (int*) calloc(primecount, sizeof(int)); 
	int* primes = buildPrimes(max);
	buildPrimeList(primes, primelist, primecount);

	for (int i = 0; i < primecount / 10; ++i) { 
		int prime = 0;
		int result = findPrime(primes, primelist, maxprime, i, &prime);
		if (result > count) {
			count = result;
			maxprime = prime;
		}

	}

	printf("%d\n", maxprime);
	free(primelist);
	return 0;
}

int findPrime(int* primes,
		const int* primelist,
		const int maxprime,
		const int start,
		int* prime) {
	int sum = 0; 
	int count = -1;
	int i;
	for (i = start; sum < 1e6; ++i) {
		sum += primelist[i];
		++count;
	}
	sum -= primelist[i - 1];
	*prime = sum;
	if (primes[sum])
		return count;
	else 
		return 0;
}

int* buildPrimes(const int maxprime)
{
	int * primes;

	primes = (int *) calloc(maxprime, sizeof(int));

	if (primes == NULL)
		exit(12);

	/* 0 stands for: Not a prime number.
	 *	 * 1 stands for: Is a prime number. */

	for ( int i = 0; i < maxprime; primes[i++] = 1)  
		;

	/* Generate prime number array. */
	primes[0] = 0;
	primes[1] = 0;
	for ( int i = 2; i < maxprime; ++i)
		for ( int j = 2; j * i <= maxprime; ++j)
			primes[j * i] = 0;

	return primes;
}

void buildPrimeList(int* primes, int* primelist, int primecount)
{
	int j = 0;
	for (int i = 0; j < primecount; ++i) {
		if (primes[i])
			primelist[j++] = i;
	}
}
