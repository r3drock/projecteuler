/* Find the Product of digit 1, 10, 100, 1000, 10000, 100000, 1000000, 10000000
 * of the Champernowne's constant */

#include <stdio.h>

int numdigits(int number);
int pickdigit(int number, int n, int len);

int main()
{
	int product = 1;
	int isearch = 10;
	int iprev = 0;
	int icurr = 1;
	int number = 1;

	while(1) {
		int dc = numdigits(++number);
		iprev = icurr;
		icurr += dc;
		if (icurr >= isearch) {
			product *= pickdigit(number, isearch - iprev, dc);
			isearch *=10;
			if (isearch > 1000000)
				break;
		}
	}
	printf("%d\n", product);
	return 0;
}

int numdigits(int number)
{
	int digitscount = 0;
	for (;number > 0; number /= 10)
		digitscount++;
	return digitscount;
} 

int pickdigit(int number, int n, int len)
{
	for(n = len - n; n > 0; --n)
		number /= 10;
	return number %10;
}
