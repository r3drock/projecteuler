/* How many Sundays fell on the first of the month 
 * during the twentieth century (1 Jan 1901 to 31 Dec 2000)? */

#include <stdio.h>

enum weekd {monday = 1, tuesday, wednesday, thursday, friday, saturday, sunday};
enum monthposinyear {january = 1, february = january + 31,
					 march = february + 28, april = march + 31,
					 may = april + 30, june = may + 31,
					 july = june  + 30, august = july  + 31,
					 september = august + 31, october = september + 30,
					 november = october + 31, december = november + 30,
					 lmarch = february + 29, lapril = lmarch + 31,
					 lmay = lapril + 30, ljune = lmay + 31,
					 ljuly = ljune  + 30, laugust = ljuly  + 31,
					 lseptember = laugust + 31, loctober = lseptember + 30,
					 lnovember = loctober + 31, ldecember = lnovember + 30};

//calculate the weekday of the first january of the given year
int gauss(int A) {
	//  w is the encoded weekday
	//	1	   2		3		  4		   5	  6		   0
	//	monday tuesday	wednesday thursday friday saturday sunday
	int w = (1 + (5 * ((A - 1) % 4)) +
			(4 * ((A - 1) % 100)) +
			(6 * (A - 1) % 400)) % 7;

	if (w == 0)
		return sunday;
	return w;
}

struct retval {
	int numbersundays;
	enum weekd weekday;
};

struct retval iterateyear(int year, enum weekd currentweekday) {
	int numbersundays = 0;
	int daysinyear = 365;
	int leapyear = 0;

	//is Leap Year?
	if (year % 4 == 0 && ((year % 100) != 0 || year % 400 == 0)) {
		leapyear = 1;
		daysinyear = 366;
	}

	for (int i = 1; i <= daysinyear; i++) {
		if (currentweekday == sunday) {
			if (leapyear == 0) {
				if (i == january || i == february || i == march    || 
						i == april   || i == may      || i == june     ||
						i == july    || i == august   || i == september||
						i == october || i == november || i == december) {
					numbersundays++;
				}
			} else {
				if (i == january  || i == february  || i == lmarch    || 
						i == lapril   || i == lmay      || i == ljune     ||
						i == ljuly    || i == laugust   || i == lseptember||
						i == loctober || i == lnovember || i == ldecember) {
					numbersundays++;
				}
			}
			currentweekday = monday;
		} else {
			currentweekday++;
		}
	}
	struct retval val;
	val.numbersundays = numbersundays;
	val.weekday = currentweekday;
	return val;
}

int main()
{
	int numbersundays = 0;
	enum weekd currentweekday = monday;
	struct retval temp = iterateyear(1900, currentweekday);
	currentweekday = temp.weekday;

	for (int currentyear = 1901; currentyear <= 2000; currentyear++) {
		temp = iterateyear(currentyear, currentweekday);
		numbersundays += temp.numbersundays;
		currentweekday = temp.weekday;
	}

	printf("%d\n", numbersundays);
	return 0;
}
