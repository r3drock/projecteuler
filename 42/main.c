/* How many words from the file words.txt are triangle words? */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FILENAME "words.txt"
#define LEN 100

int triangleNumbers[LEN];

int isTriangleNumber(int wordSum);
int strcomp(const void * a, const void * b);
int countTriangleNumbers();


int main()
{
	for (int i = 1; i < LEN; ++i)
		triangleNumbers[i - 1] = (i * (i + 1)) / 2;
	printf("%d\n", countTriangleNumbers());
	return 0;
}

int isTriangleNumber(int wordSum)
{
	for (int i = 0; i < LEN; ++i) {
		if (triangleNumbers[i] == wordSum)
			return 1;
	}
	return 0;
}

int countTriangleNumbers()
{
	FILE *fp;
	fp = fopen(FILENAME,"r");
	if (fp == NULL)
		return -1;

	int triangleNumberCount = 0;
	int wordSum;
	int c;
	while ((c = getc(fp)) != EOF) {
		wordSum = 0;
		if (c == '\"') {
			while ((c = getc(fp)) != EOF && (c != '\"')) {
				wordSum += (c - 'A') + 1;
			}
			triangleNumberCount+= isTriangleNumber(wordSum);
		}
	}

	fclose(fp);
	return triangleNumberCount;
}
