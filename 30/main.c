/* How many different ways can £2 be made using any number of coins? */

#include <stdio.h>

int calc(int number) {
	int retval = 0; 
	while (number > 0) {
		int temp = number % 10;
		retval += temp * temp * temp * temp * temp;
		number /= 10;
	}
	return retval;
}

int main()
{
	int sum = 0;

	for (int i = 2; i < (9 * 9 * 9 * 9 * 9 * 6); ++i) {
		if (calc(i) == i)
			sum += i;
	}
	printf("%d\n", sum);
	return 0;
}

