/* Find the second smallest triangle number that is also pentagonal and
 * hexagonal. */

#include <stdio.h>
#include <stdlib.h>

long triangle(long t)
{
	return t * (t + 1) / 2;
}

long pentagonal(long p)
{
	return p * ((3 * p) - 1) / 2;
}

long hexagonal(long h)
{
	return h * ((2 * h) - 1);
}

int main()
{
	long result;
	long t = 1;
	long p = 1;
	long h = 1;

	while (1) {
		long tt = triangle(t);
		long pp = pentagonal(p);
		long hh = hexagonal(h);
		if (tt == pp  &&
			tt == hh &&
			pp > 40755) {
			result = tt;
			break;
		} else if (tt < pp && tt < hh) {
			++t;
		} else if (pp < hh && pp < tt) {
			++p;
		} else if (hh < pp && hh < tt) {
			++h;
		} else if (tt < pp) {
			++t;
		} else if (pp < tt) {
			++p;
		} else if (hh < tt) {
			++h;
		} else if (tt == pp) {
			++p;
		} else if (hh == pp) {
			++h;
		} else if (tt == hh) {
			++t;
		} else {
			fprintf(stderr, "%ld %ld %ld\n", tt, pp, hh);
		}
	}

	printf("%ld\n", result);
	return 0;
}
