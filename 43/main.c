/* Find the sum of all 0 to 9 pandigital number with a special property */

#include <stdio.h>
#include <stdlib.h>

long sum = 0;

long concat(int digits[10]);
int isDivisible(long number);
void permute(int number[10], int depth);

int main()
{
	int digits[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	permute(digits, 0);

	printf("%ld\n", sum);
	return 0;
}

void permute(int digits[10], int depth)
{
	if (depth == 9) {
		long number = concat(digits);
		if (isDivisible(number))
			sum += number;
		return;
	}
	int* numbertemp = (int *) malloc(sizeof(int) * 10);
	if (numbertemp == NULL)
		exit(12);
	for (int i = 0; i < 10; ++i)
		numbertemp[i] = digits[i];

	for (int i = depth; i < 10; ++i) {
		int temp = numbertemp[depth];
		numbertemp[depth] = numbertemp[i]; 
		numbertemp[i] = temp; 
		permute(numbertemp, depth + 1);
	}
	free(numbertemp);
}

long concat(int digits[10])
{
	long number = 0;
	long multiplicand = 1;
	for (int i = 10 - 1; i >= 0; --i) {
		number += digits[i] * multiplicand; 
		multiplicand *= 10;
	}
	return number;
}

int isDivisible(long number)
{
	int primelist[] = {17, 13, 11, 7, 5, 3, 2};
	int i = 0;
	int max = sizeof(primelist) / sizeof(primelist[0]);

	int digit3 = number % 10;
	number /= 10;
	int digit2 = number % 10;
	number /= 10;
	int digit1 = number % 10;
	number /= 10;

	for (; i < max; ++i) {
		if ((100 * digit1 + 10 * digit2 + digit3) % primelist[i] != 0)
			return 0;
		digit3 = digit2;
		digit2 = digit1;
		digit1 = number % 10;
		number /= 10;
	}
	return 1;
}
