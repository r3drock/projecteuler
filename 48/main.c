/* Find the last ten digits of the series */

#include <stdio.h>
#include <gmp.h>

int main()
{
	mpz_t val;
	mpz_t temp;

	mpz_init(val);
	mpz_init(temp);

	mpz_ui_pow_ui(val, 1, 1);	

	for (unsigned int i = 2; i <= 1000; ++i) {
		mpz_ui_pow_ui(temp, i, i);
		mpz_add(val, val, temp);
	}
	
	int digits[10];
	for (int i = 9; i >= 0; --i) {
		digits[i] = mpz_fdiv_ui(val, 10); 
		mpz_div_ui(val, val, 10);
	}
	for (int i = 0; i < 10; ++i) {
		printf("%d", digits[i]);
	}
	printf("\n");

	mpz_clear(temp);
	mpz_clear(val);
	return 0;
}
