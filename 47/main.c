/* Which prime, below one-million, can be written as the sum of the most? */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

long* buildPrimes(const size_t max);
long countPrimeFactors(long* primelist, long number);
long* buildPrimelist(long* primes, size_t max);

int main()
{
	const size_t max = 1e6;
	long* primes = buildPrimes(max);
	long* primelist = buildPrimelist(primes, max);
	int count = 0;
	char prevhasfourfactors = 0;
	long i = 2 * 3 * 5 * 7;
	while (count < 4 && i < max - 1) {
		++i;
		if(primes[i]) {
			count = 0;
		} else {
			long temp = countPrimeFactors(primelist, i);
			if (prevhasfourfactors && temp == 4) {
				++count;	
			} else if (temp == 4) {
				count = 1;
				prevhasfourfactors = 1;
			} else {
				prevhasfourfactors = 0;
				count = 0;
			}
		}
	}
	printf("%ld\n", i - 3);
	return 0;
}

long* buildPrimes(const size_t max)
{
	long * primes;

	primes = (long *) calloc(max, sizeof(long));

	if (primes == NULL)
		exit(12);

	/* 0 stands for: Not a prime number.
	 *	 * 1 stands for: Is a prime number. */

	for ( long i = 0; i < max; primes[i++] = 1)
		;

	/* Generate prime number array. */
	primes[0] = 0;
	primes[1] = 0;
	for ( size_t i = 2; i < max; ++i)
		for ( size_t j = 2; j * i <= max; ++j)
			primes[j * i] = 0;

	return primes;
}

long* buildPrimelist(long* primes, size_t max) {
	int count = 0;
	for (int i = 0; i < max; ++i) {
		count += primes[i];
	}
	long* primelist = malloc(sizeof(long) * count);
	if (primelist == NULL)
		exit(12);
	int j = 0;
	for (int i = 0; i < max; ++i)
		if (primes[i])
			primelist[j++] = i;
	return primelist;
}

long countPrimeFactors(long* primelist, long number) {
	int count = 0;
	int i = 0;
	while (number > 1) {
		if ((number % primelist[i]) == 0) {
			++count;
			do {
				number /= primelist[i];
			} while ((number % primelist[i]) == 0);
		}
		++i;
	}
	return count;
}
