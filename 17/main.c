/* If all the numbers from 1 to 1000 (one thousand) inclusive 
 * were written out in words, how many letters would be used?*/ 

#include <stdio.h>

enum {null = 0, one = 3, two = 3, three = 5, four = 4, five = 5,
				six = 3, seven = 5, eight = 5, nine = 4, ten = 3,
				eleven = 6, twelve = 6, thirteen = 8, fourteen = 8,
				fifteen = 7, sixteen = 7, seventeen = 9, eighteen = 8,
				nineteen = 8, twenty = 6, thirty = 6, forty = 5,
				fifty = 5, sixty = 5, seventy = 7, eighty = 6,
				ninety = 6, hundred = 7, and = 3};
int numbers_0_20[19] = {null, one, two, three, four,
	five, six, seven, eight, nine, ten, eleven, twelve, thirteen,
	fourteen, fifteen, sixteen, eighteen, nineteen};
int numbers_0_100_10[10] = {null, ten, twenty, thirty, forty, fifty,
						sixty, seventy, eighty, ninety};

/* Get lettercount in a number */
int get_letter_count(int num)
{
	int sum = 0;
	int digits[3] = {0,0,0};

	if (num > 1000 || num < 1)
		return -1;
	if (num == 1000)
		return 11;

	if (num % 100 != 0 && num > 100)
		sum += and;
	if (num >=100)
		sum += hundred;

	for (int i = 2; i >= 0 ; --i) {
		digits[i] = num % 10;
		num /= 10;
	}

	//X00
	sum += numbers_0_20[digits[0]];
	//0X0
	if (digits[1] < 2) {
		sum += numbers_0_20[(digits[1] * 10) + digits[2]];
		return sum;
	} else {
		sum += numbers_0_100_10[digits[1]];
	}
	//00X
	sum += numbers_0_20[digits[2]];
	return sum;
}

int main()
{
	int sum = 0;
	for (int i = 1; i <= 1000; ++i)
		sum += get_letter_count(i);

	printf("%d\n", sum);

	return 0;
}
