/*Starting in the top left corner of a 2×2 grid, and only
 *being able to move to the right and down, there are exactly
 *6 routes to the bottom right corner.
 *How many such routes are there through a 20×20 grid? */

#define MAX 20

#include <stdio.h>
unsigned long long countmemory[MAX][MAX];

unsigned long long routescount(int x, int y)
{
	unsigned long long result;

	if (x ==  MAX || y == MAX) {
		result = 1;
	} else if (x + 1 == MAX) {
		result = 1 + routescount(x, y + 1);
	} else if (y + 1 == MAX) {
		result = routescount(x + 1, y) + 1;
	} else if (countmemory[x + 1][y]) {
		if (countmemory[x][y + 1]) {
			result = countmemory[x + 1][y] + countmemory[x][y + 1];
		} else {
			result = countmemory[x + 1][y] + routescount(x, y + 1);
		}
	} else if (countmemory[x][y + 1]) {
		result = routescount(x + 1, y) + countmemory[x][y + 1];
	} else {
		result = routescount(x + 1, y) + 
			routescount(x, y + 1);
	}
	countmemory[x][y] = result;
	return result;
}

int main()
{
	printf("%llu\n", routescount(0, 0));
	return 0;
}
