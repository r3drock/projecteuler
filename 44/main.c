/* Find the pair of pentagonal numbers, Pj and Pk, for which their sum and
 * difference are pentagonal and D = |Pk − Pj| is minimised;
 * what is the value of D? */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>

#define SIZE 5000

void generate(long pentagonals[]);
int isPentagonal(long number);

int main()
{
	long difference = LONG_MAX;
	long pentagonals[SIZE];

	generate(pentagonals);

	for (int i = 0; i < SIZE; ++i) {
		for (int j = i + 1; j < SIZE; ++j) {
			long D = pentagonals[j] - pentagonals[i];
			if (isPentagonal(D) &&
					isPentagonal(pentagonals[j] + pentagonals[i]) &&
					difference > D) {
				difference = D;
				break;
			}
		}
	}
	printf("%ld\n", difference);
	return 0;
}

void generate(long pentagonals[])
{
	for (int i = 0; i < SIZE; ++i) {
		pentagonals[i] = (i + 1) * ((3 * (i + 1)) - 1) / 2;
	}
}

int isPentagonal(long number)
{
	double num = (double)  (24 * number) + 1;
	num = (sqrt(num) + 1) / 6;
	return fmod(num, 1.0) == 0.0;
}
