set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wno-long-long -pedantic -lm")
set( CMAKE_EXPORT_COMPILE_COMMANDS ON )

add_executable(44.out main.c)
target_link_libraries(44.out m)
add_custom_target(run44
	COMMAND 44.out 
	DEPENDS 44.out
	WORKING_DIRECTORY ${CMAKE_PROJECT_DIR}
)
