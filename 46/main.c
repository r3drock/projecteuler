/* What is the smallest odd composite that cannot be written as the sum of
 * a prime and twice a square? */

#include <stdio.h>
#include <stdlib.h>

int isWriteableAsGoldbach();
long * buildPrimes();

int main()
{
	long result;
	long * primes = buildPrimes (1);
	long i = 3;

	for (size_t maxprime = 2e2;; maxprime *= 2) {
		free(primes); 
		primes = buildPrimes(maxprime);
		for(; i < maxprime; i += 2) {
			if (primes[i])
				continue;
			if (!isWriteableAsGoldbach(i, primes)) {
				result = i;
				goto end;
			}
		}
	}

end:
	printf("%ld\n", result);
	return 0;
}

// Tells for an odd composite number whether it can be written
// as a sum of a prime and twice a square
int isWriteableAsGoldbach(long number, long* primes)
{
	for (long iprime = 2; iprime < number; ++iprime) {
		if (!primes[iprime])
			continue;
		for (long s = 1; iprime + 2 * s * s <= number; s++) {
			if (iprime + 2 * s * s == number) {
				return 1;
			}
		}
	}
	return 0;
}

long * buildPrimes(const size_t maxprime)
{
	long * primes;

	primes = (long *) calloc(maxprime,sizeof(long));

	if (primes == NULL)
		exit(12);
	/* 0 stands for: Not a prime number.
	 * 1 stands for: Is a prime number. */

	for ( long i = 0; i < maxprime; primes[i++] = 1)  
		;

	/* Generate prime number array. */
	primes[0] = 0;
	primes[1] = 0;
	for ( long i = 2; i < maxprime; ++i)
		for ( long j = 2; j * i <= maxprime; ++j)
			primes[j * i] = 0;

	return primes;
}
