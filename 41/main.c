/* What is the largest n-digit pandigital prime that exists? */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int largest = 0;

void permute(int* digits, int numdigits, int depth);
int isPrime(unsigned int number) ;
long concat(int* digits, int numdigits);

int main()
{
	int digits[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

	for (int numdigits = 4; numdigits < 9; ++numdigits) 
		permute(digits, numdigits, 0);

	printf("%d\n", largest);
	return 0;
}

void permute(int* digits, int numdigits, int depth)
{
	if (depth == numdigits - 1) {
		long number = concat(digits, numdigits);
		if (isPrime(number)) 
			if (largest < number)
				largest = number;
		return;
	}
	int* numbertemp = (int *) malloc(sizeof(int) * numdigits);
	if (numbertemp == NULL)
		exit(12);
	for (int i = 0; i < numdigits; ++i)
		numbertemp[i] = digits[i];

	for (int i = depth; i < numdigits; ++i) {
		int temp = numbertemp[depth];
		numbertemp[depth] = numbertemp[i]; 
		numbertemp[i] = temp; 
		permute(numbertemp, numdigits, depth + 1);
	}
	free(numbertemp);
}

int isPrime(unsigned int number) 
{
	int limit = (int) sqrt(number);

	for (int i = 2; i < limit; ++i)
		if (number % i == 0)
			return 0;
	return 1;
}

long concat(int* digits, int numdigits)
{
	long number = 0;
	long multiplicand = 1;
	for (int i = numdigits - 1; i >= 0; --i) {
		number += digits[i] * multiplicand; 
		multiplicand *= 10;
	}
	return number;
}

