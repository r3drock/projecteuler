
#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <math.h>

#define SIZE 2000

void toArray(mpz_t number, int* digits)
{
	mpz_t temp;
	mpz_init(temp);
	for (int i = SIZE - 1; i >= 0; --i) {
		mpz_set(temp, number);
		mpz_div_ui(number, number, 10);
		mpz_mul_ui(number, number, 10);
		mpz_sub(temp, temp, number);
		mpz_div_ui(number, number, 10);
		int digit = mpz_get_si(temp);
		digits[i] = digit;
	}
}

int testHypothese(int* digits, int hypothese[SIZE], int hyplen, int index) {
	for (int i = 0; hyplen > 0; ++i) {
		--hyplen;
		if (digits[i] != hypothese[i]) {
			return 0;
		}
	}
	return 1;
}

int countrecurringDigits(mpz_t number)
{
	int digits[SIZE];
	toArray(number, digits);
	int hypothese[SIZE];
	int successfultest = 0;
	int hyplen;

	for (int k = 1; k < SIZE; ++k) {
		hypothese[0] = digits[k - 1];
		hyplen = 1;
		int onlyzerosleft = 1;
		for (int j = k - 1; j < SIZE; ++j) {
			if (digits[j] != 0) {
				break;
				onlyzerosleft = 0;
			}
		}
		if (!onlyzerosleft)
			break;
		for (int i = k; i < SIZE; ++i) {
			if (i + hyplen >= SIZE)
				break;
			if (testHypothese(digits + i, hypothese, hyplen, i)) {
				i += hyplen - 1;
				successfultest = 1;
			} else {
				hypothese[hyplen++] = digits[i];
				successfultest = 0;
			}
		}
		if (successfultest) {
			break;
		}
	}
	if ((hyplen < (SIZE - 1)) && successfultest) {
		return hyplen;
	}
	else  {
		return -1;
	}
}

int main()
{
	int max = 0;
	int maxcount = 0;

	mpz_t number;
	mpz_init(number);

	for (int i = (SIZE / 2) - 1; i >= (maxcount - 1) && i > 2; --i){
		mpz_ui_pow_ui(number, 10, SIZE);
		mpz_div_ui(number, number, i);
		int tmp = countrecurringDigits(number);
		if (tmp > maxcount) {
			maxcount = tmp;
			max = i;
		}
	}

	printf("%d\n", max);
	return 0;
}
