/* What is the concatenation of the three numbers except 1487, 4817 and 8147
 * which follow these properties:
 * (i)each of the three terms are prime
 * (ii) each of the 4-digit numbers are permutations of one another. */

#include <stdio.h>
#include <stdlib.h>

#define MAXPRIME 10000

int arePermutations(int number1, int number2);
long * buildPrimes();

int main()
{
	long * primes = buildPrimes();

	for (int i = 1000; i < MAXPRIME; ++i) {
		if (primes[i] == 0)
			continue;
		for (int j = i + 1; j < MAXPRIME; j += 1) {
			if (primes[j] == 0)
				continue;
			int k = j + j - i;
			if (k > MAXPRIME) {
				j+= MAXPRIME;
				continue;
			}
			if (primes[k] == 0)
				continue;
			if (arePermutations(i, j) && arePermutations(i, k)) {
				printf("%d%d%d\n", i, j, k);
			}
		}
	}
	return 0;
}

int arePermutations(int number1, int number2) {
	int bitmask[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; 
	int bitmask2[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; 

	for(int i = 0; i < 4; ++i) {
		int digit = number1 % 10;
		number1 /= 10;
		++bitmask[digit];
		digit = number2 % 10;
		number2 /= 10;
		++bitmask2[digit];
	}

	for (int i = 0; i < 10; ++i)
		if (bitmask[i] != bitmask2[i])
			return 0;
	return 1;
}

long * buildPrimes()
{
	long * primes;

	primes = (long *) calloc(MAXPRIME,sizeof(long));

	if (primes == NULL)
		exit(12);
	/* 0 stands for: Not a prime number.
	 * 1 stands for: Is a prime number. */

	for ( long i = 0; i < MAXPRIME; primes[i++] = 1)  
		;

	/* Generate prime number array. */
	primes[0] = 0;
	primes[1] = 0;
	for ( long i = 2; i < MAXPRIME; ++i)
		for ( long j = 2; j * i < MAXPRIME; ++j)
			primes[j * i] = 0;

	return primes;
}

